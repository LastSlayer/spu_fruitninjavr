using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Blade : MonoBehaviour
{
    bool isPickUp;
    public ControllerSide holderSide;
    private void OnTriggerStay(Collider other)
    {
        Debug.Log("TOUCH " + other.name);
        if (isPickUp)
        {
            // CHECK MENU SLASH
            if (GameManager.Instance.currentGameMode == GameMode.MainMenu)
            {
                if(other.CompareTag("UI_Standard") && ((holderSide == ControllerSide.Left && GameManager.Instance.LeftTrigger) || (holderSide == ControllerSide.Right && GameManager.Instance.RightTrigger)))
                {
                    GameManager.Instance.StartGame_Standard();
                }
                else if (other.CompareTag("UI_TimeAttack") && ((holderSide == ControllerSide.Left && GameManager.Instance.LeftTrigger) || (holderSide == ControllerSide.Right && GameManager.Instance.RightTrigger)))
                {
                    GameManager.Instance.StartGame_TimeAttack();
                }
                else if(other.CompareTag("UI_Quit") && ((holderSide == ControllerSide.Left && GameManager.Instance.LeftTrigger) || (holderSide == ControllerSide.Right && GameManager.Instance.RightTrigger)))
                {
                    GameManager.Instance.QuitGame();
                }
            }
            else
            {
                // CHECK FRUIT SLASH
                if (other.CompareTag("GameObject/Fruit") && ((holderSide == ControllerSide.Left && GameManager.Instance.LeftTrigger) || (holderSide == ControllerSide.Right && GameManager.Instance.RightTrigger)))
                {
                    GameManager.Instance.AddScore(1.0f);
                    Instantiate(GameManager.Instance.fruitHitParticle, other.transform.position, other.transform.rotation, null);
                    Destroy(other.gameObject);
                }
                else if (other.CompareTag("GameObject/Bomb") && ((holderSide == ControllerSide.Left && GameManager.Instance.LeftTrigger) || (holderSide == ControllerSide.Right && GameManager.Instance.RightTrigger)))
                {
                    GameManager.Instance.DecreaseHP(1);
                    Instantiate(GameManager.Instance.bombHitParticle, other.transform.position, other.transform.rotation, null);
                    Destroy(other.gameObject);
                }
            }
        }
        else
        {
            // CHECK PICKUP 
            if (other.CompareTag("LeftController") && GameManager.Instance.LeftTrigger)
            {
                isPickUp = true;
                GameManager.Instance.PickUpBlade_LeftHand(this.transform);
                holderSide = ControllerSide.Left;
            }
            else if (other.CompareTag("RightController") && GameManager.Instance.RightTrigger)
            {
                isPickUp = true;
                GameManager.Instance.PickUpBlade_RightHand(this.transform);
                holderSide = ControllerSide.Right;
            }
        }
    }

    
}
