using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum GameMode
{
    MainMenu,
    Standard,
    TimeAttack
}

public enum ControllerSide
{
    Left,
    Right
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public GameMode currentGameMode = GameMode.MainMenu;
    private Coroutine currentGameModeRoutine;
    private Coroutine countdownRoutine;
    private float currentScore;
    private void Awake()
    {
        Instance = this;
    }

    #region INPUT

    private bool leftTrigger;
    private bool leftGrip;
    private bool rightTrigger;
    private bool rightGrip;

    public bool LeftTrigger => leftTrigger;
    public bool LeftGrip => leftGrip;
    public bool RightTrigger => rightTrigger;
    public bool RightGrip => rightGrip;

    public void OnLeftTrigger(bool _bool)
    {
        leftTrigger = _bool;
    }

    public void OnLeftGrip(bool _bool)
    {
        leftGrip = _bool;
    }

    public void OnRightTrigger(bool _bool)
    {

        Debug.Log("RIGHT TRIGGER");
        rightTrigger = _bool;
    }

    public void OnRightGrip(bool _bool)
    {
        Debug.Log("RIGHT GRIP");
        rightGrip = _bool;
    }
    #endregion

    #region HP
    public GameObject[] hpObject;
    private int currentHP;
    public int CurrentHp => currentHP;

    public void InitializeHP()
    {
        currentHP = hpObject.Length;
    }

    public void ResetHP()
    {
        foreach(var _hp in hpObject)
        {
            _hp.SetActive(true);
        }
        currentHP = hpObject.Length;
    }

    public void DecreaseHP(int _amount)
    {
        if (_amount > hpObject.Length) _amount = hpObject.Length;
        currentHP -= _amount;
        UpdateHP(currentHP);
    }

    public void UpdateHP(int _currentAmount)
    {
        var _updateAmount = hpObject.Length - _currentAmount;

        for(int i = 0; i < _updateAmount; i ++)
        {
            hpObject[i].SetActive(false);
        }
    }
    #endregion

    #region GAME MODE
    public Transform[] fruitSpawner;
    public Rigidbody[] fruitObject;
    public Rigidbody bombObject;
    public Vector2 spawnDelayInterval = new Vector2( 1, 3);
    public Vector2 spawnDelay = new Vector2(0.0f, 0.5f);
    public Vector2 spawnerAddForce = new Vector2( 1.0f, 1.5f);
    public Vector2 spawnQuantity = new Vector2( 1, 6);
    public Vector2 bombQuantity = new Vector2(0, 3);

    [Header("TIME ATTACK SETTINGS")]
    public TMPro.TMP_Text timeText;
    public float timeAttackSeconds = 120.0f;
    private float currentTime;
    private List<Rigidbody> currentSpawnList = new List<Rigidbody>();

    IEnumerator SpawnLoop_Standard()
    {
        while(currentHP > 0)
        {
            yield return new WaitForSeconds(Random.Range(spawnDelayInterval.x, spawnDelayInterval.y));
            var _FruitQuantity = Random.Range((int)spawnQuantity.x, (int)spawnQuantity.y);
            var _BombQuantity = Random.Range((int)bombQuantity.x, (int)bombQuantity.y);
            SetupSpawnList(_FruitQuantity, _BombQuantity);
            foreach(var _spawnObj in currentSpawnList)
            {
                if (currentHP <= 0) break;
                var _SpawnFruit = Instantiate(_spawnObj, fruitSpawner[Random.Range(0, fruitSpawner.Length)].position, Quaternion.identity, null);
                _SpawnFruit.AddForce(new Vector3( 0, Random.Range(spawnerAddForce.x, spawnerAddForce.y), 0), ForceMode.Impulse);
                yield return new WaitForSeconds(Random.Range(spawnDelay.x, spawnDelay.y));
            }
        }
        BackToMainMenu();
        currentGameModeRoutine = null;
    }

    IEnumerator SpawnLoop_TimeAttack()
    {
        timeText.gameObject.SetActive(true);
        currentTime = timeAttackSeconds;
        countdownRoutine = StartCoroutine(CountdownRoutine_TimeAttack());
        while (currentTime > 0)
        {
            yield return new WaitForSeconds(Random.Range(spawnDelayInterval.x, spawnDelayInterval.y));
            var _FruitQuantity = Random.Range((int)spawnQuantity.x, (int)spawnQuantity.y);
            SetupSpawnList(_FruitQuantity, 0);
            foreach (var _spawnObj in currentSpawnList)
            {
                if (currentTime <= 0) break;
                var _SpawnFruit = Instantiate(_spawnObj, fruitSpawner[Random.Range(0, fruitSpawner.Length)].position, Quaternion.identity, null);
                _SpawnFruit.AddForce(new Vector3(0, Random.Range(spawnerAddForce.x, spawnerAddForce.y), 0), ForceMode.Impulse);
                yield return new WaitForSeconds(Random.Range(spawnDelay.x, spawnDelay.y));
            }
        }
        BackToMainMenu();
        currentGameModeRoutine = null;
    }

    IEnumerator CountdownRoutine_TimeAttack()
    {
        while(currentTime > 0.0f)
        {
            currentTime -= Time.deltaTime;
            timeText.text = ((int)currentTime).ToString();
            yield return null;
        }
        countdownRoutine = null;
    }

    private void SetupSpawnList(int _FruitQuantity, int _BombQuantity)
    {
        List<Rigidbody> newSpawnList = new List<Rigidbody>();

        for(int i = 0; i < _FruitQuantity; i++)
        {
            newSpawnList.Add(fruitObject[Random.Range(0, fruitObject.Length)]);
        }
        for(int i = 0; i< _BombQuantity; i++)
        {
            newSpawnList.Add(bombObject);
        }
        currentSpawnList.Clear();
        while (newSpawnList.Count > 0)
        {
            int index = Random.Range(0, newSpawnList.Count); //pick a random item from the master list
            currentSpawnList.Add(newSpawnList[index]); //place it at the end of the randomized list
            newSpawnList.RemoveAt(index);
        }
    }
    #endregion

    #region BLADE PICK
    public Transform rightBladeHolder;
    public Transform leftBladeHolder;

    public GameObject fruitHitParticle;
    public GameObject bombHitParticle;
    bool rightBladeHold;
    bool leftBladeHold;
    public void PickUpBlade_LeftHand(Transform blade)
    {
        if (leftBladeHold) return;
        leftBladeHold = true;
        blade.parent = leftBladeHolder;
        blade.localPosition = Vector3.zero;
        blade.localRotation = Quaternion.Euler(Vector3.zero);
    }

    public void PickUpBlade_RightHand(Transform blade)
    {
        if (rightBladeHold) return;
        rightBladeHold = true;
        blade.parent = rightBladeHolder;
        blade.localPosition = Vector3.zero;
        blade.localRotation = Quaternion.Euler(Vector3.zero);
    }
    #endregion

    #region UI FUNCTION
    public GameObject uiButtonGroup;
    public TMPro.TMP_Text scoreText;
    public void StartGame_Standard()
    {
        ResetHP();
        ResetScore();
        uiButtonGroup.SetActive(false);
        currentGameMode = GameMode.Standard;
        currentGameModeRoutine = StartCoroutine(SpawnLoop_Standard());
    }

    public void StartGame_TimeAttack()
    {
        ResetHP();
        ResetScore();
        uiButtonGroup.SetActive(false);
        currentGameMode = GameMode.TimeAttack;
        currentGameModeRoutine = StartCoroutine(SpawnLoop_TimeAttack());
    }

    public void ResetScore()
    {
        currentScore = 0.0f;
        UpdateScore();
    }

    // CAll by fruit object upon blade slashed
    public void AddScore(float _amount)
    {
        currentScore += _amount;
        UpdateScore();
    }

    public void UpdateScore()
    {
        scoreText.text = currentScore.ToString();
    }

    public void BackToMainMenu()
    {
        currentGameMode = GameMode.MainMenu;
        timeText.gameObject.SetActive(false);
        uiButtonGroup.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    #endregion
}
